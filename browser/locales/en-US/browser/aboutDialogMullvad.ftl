# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

mullvad-about-desc = { -brand-short-name }  is a privacy-focused web browser developed in collaboration between <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> and the <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. It’s produced to minimize tracking and fingerprinting.
mullvad-about-readMore = Read more
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetry
